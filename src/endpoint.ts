import { Request, Response } from "express";
import APIError from "./api-error";

/**
 * Endpoints process a request by providing a response.
 * No other handler processes a request after the endpoint.
 */
export default interface Endpoint {

	/** Handles an HTTP GET request to this endpoint. */
	get?: (req: Request, res: Response) => Promise<void>

	/** Handles an HTTP POST request to this endpoint. */
	post?: (req: Request, res: Response) => Promise<void>

	/** Handles an HTTP PUT request to this endpoint. */
	put?: (req: Request, res: Response) => Promise<void>

	/** Handles an HTTP DELETE request to this endpoint. */
	delete?: (req: Request, res: Response) => Promise<void>

	/** Handles an HTTP PATCH request to this endpoint. */
	patch?: (req: Request, res: Response) => Promise<void>

}
