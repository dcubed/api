import Middleware from "./middleware";
import { Request, Response } from "express";
import * as path from "path";
import Endpoint from "./endpoint";
import APIError from "./api-error";
import { CORSOptions, CORSMiddleware } from ".";
import * as fs from "fs";
import { resolve } from "url";


/** An API is a collection of endpoints and middleware that implement an API. */
export default class API extends Middleware {

	/** The CORs middleware to run if specified */
	private readonly corsHandler: Middleware;

	/** Construct given the API's root. */
	constructor(
		private root: string,
		options?: Partial<APIOptions>
	) {
		super();
		this.consumes = true;

		if(options != null && options.cors != null) {
			this.corsHandler = new CORSMiddleware(options.cors);
		}
	}

	/** Run selects the correct middlewares and endpoints to run. */
	public async run(req: Request, res: Response): Promise<void> {
		res.header('Cache-Control', 'no-cache');

		// Run the CORs handler if there is one
		if(this.corsHandler != null) {
			await this.corsHandler.run(req, res);
			if(req.method == 'OPTIONS') {
				res.status(200).json({});
				return;
			}
		}

		let endpointPath = path.join(this.root, req.path);
		if(req.path == '/') {
			throw new APIError(404, 'endpiont not specified in URL');
		}
		if(endpointPath[endpointPath.length-1] == '/') {
			endpointPath = endpointPath.substring(0, endpointPath.length-1);
		}

		await new Promise<void>((resolve, reject) => fs.stat(`${endpointPath}.js`, err => {
				if(err) {
					if(err.code === 'ENOENT') {
						reject(new APIError(404, `endpoint not found at ${req.path}`));
					}
					reject(err);
				}
				resolve();
			})
		);

		let endpoint: Endpoint;
		try {
			endpoint = require(endpointPath);
		} catch(err) {
			throw new APIError(500, `Error in endpoint ${req.path}:` + '\n' + err + (err.stack ? '\n' + err.stack : ''));
		}

		let handler: (req: Request, res: Response) => Promise<void>;
		switch(req.method) {
			case 'GET':
				handler = endpoint.get;
				break;
			case 'POST':
				handler = endpoint.post;
				break;
			case 'PUT':
				handler = endpoint.put;
				break;
			case 'DELETE':
				handler = endpoint.delete;
				break;
			case 'PATCH':
				handler = endpoint.patch;
				break;
		}
		if(handler == null) {
			throw new APIError(405, `method not supported: ${req.method}`);
		}
		
		await handler(req, res);

		if(res.headersSent != true) {
			res.json({});
		}
	}

}

/** Options for the API */
export interface APIOptions {

	/** CORs options, to enable CORs headers and pre-flight request handling */
	cors: CORSOptions;

}
