
/**
 * An API error is thrown to control the HTTP status code and message.
 */
export default class APIError extends Error {

	/** The HTTP status code of this error. */
	readonly statusCode: number;

	/** The error specific type identifier (optional). */
	readonly type: string;

	/** Construct given the the HTTP status code, message, and optional type. */
	constructor(statusCode: number, message: string, type?: string) {
		super(message);
		this.statusCode = statusCode;
		this.type = type;
		this.name = 'APIError';
	}

}
