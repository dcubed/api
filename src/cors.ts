import { Request, Response } from "express";
import { Middleware } from ".";
import { MiddlewareRequestOptions } from "./middleware";

/** Options for enabling CORS on an origin */
export interface CORSOptions {

	/** The origin to allow access from */
	origin: string;

	/** The methods that are allowed */
	methods?: string[];

	/** The custom headers that are allowed */
	headers?: string[];

}

/** Handles CORs pre-flight requests and adds the appropriate headers */
export class CORSMiddleware extends Middleware {

	/** The concatenated methods */
	private readonly methods = new ConcatenatedString(this.options.methods, ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']);

	/** The concatenated headers */
	private readonly headers: ConcatenatedString;

	constructor(
		private readonly options: CORSOptions
	) {
		super();
		
		options.headers = options.headers || [];
		this.headers = new ConcatenatedString(options.headers, ['Accept', 'Accept-Language', 'Content-Language', 'Content-Type']);
	}

	/** Adds the CORS headers */
	async run(req: Request, res: Response): Promise<MiddlewareRequestOptions> {
		res.setHeader('Access-Control-Allow-Credentials', 'true');
		res.setHeader('Access-Control-Allow-Origin', this.options.origin);
		res.setHeader('Access-Control-Allow-Methods', this.methods.value);
		if(this.headers != null) {
			res.setHeader('Access-Control-Allow-Headers', this.headers.value);
		}

		if(req.method == 'OPTIONS') {
			return { cancel: true };
		}

		return null;
	}

}

/** Concatenates a list of string */
class ConcatenatedString {
	
	/** The cached concatenated value */
	private cache: string;

	/** True if the value is cached */
	private isCached = false;

	constructor(
		private readonly list: string[],
		private readonly defaults: string[] = [],
		private readonly joiner: string = ', '
	) { }

	/** The concatenated value */
	public get value(): string {
		if(this.isCached) return this.cache;
		let allValues: string[] = [];

		if(this.list != null) {
			for(let value of this.list) {
				allValues.push(value.trim());
			}
		}

		if(this.defaults != null) {
			for(let value of this.defaults) {
				if(allValues.indexOf(value.trim()) >= 0) continue;
				allValues.push(value.trim());
			}
		}
		
		this.cache = allValues.join(this.joiner);
		this.isCached = true;

		return this.cache;
	}

}
