import { Request, Response } from "express";
import APIError from "./api-error";

/**
 * Middlewares processes a request before it is sent to the endpoint.
 */
export default abstract class Middleware {

	/** If set to true then this middleware consumes the request. */
	protected consumes: boolean = false;

	/** Performs this middleware's action on the request. */
	public abstract run(req: Request, res: Response): Promise<MiddlewareRequestOptions | void>;

	/** Creates an Express middleware function for this middleware. */
	public handler(): (req: Request, res: Response, next: () => void) => void {
		return (req: Request, res: Response, next: () => void) => {
			this.run(req, res)

				.then((middlewareOptions) => {
					middlewareOptions = middlewareOptions || {};

					if(this.consumes != true || middlewareOptions.cancel != true) {
						next();
					}
				})

				.catch((err: APIError) => {
					if(!(err instanceof APIError)) {
						console.error('');
						console.error((err as any).stack);
						err = new APIError(500, err + '');
					}
					res.status(err.statusCode).json({
						message: err.message,
						type: err.type
					});
				});
		}
	}

}

/** The data returned from a middleware run that controls how the request is handled */
export interface MiddlewareRequestOptions {
	
	/** If true then next() will not be called */
	cancel?: boolean;

}
