import API from "./api";
import { Request, Response } from "express";
import Handler from "./handler";
import Middleware from "./middleware";

/** Creates a new API mounted at the given root path. */
export function attach(root: string): Function {
	let api = new API(root);
	return api.handler();
};

/** Creates an endpoint handler. */
export function handler(f: (req: Request, res: Response) => Promise<void>): Handler {
	let beforeHandlers: Middleware[] = [];
	
	// handler
	let h = async function(req: Request, res: Response): Promise<void> {
		for(let m of beforeHandlers) {
			await m.run(req, res);
		}

		return f(req, res);
	} as Handler;

	// requires
	h.requires = function(middleware: Middleware): Handler {
		beforeHandlers.push(middleware);
		return h;
	}

	return h;
};

/** Creates an API middleware function. */
export function middleware(f: (req: Request, res: Response) => Promise<void>): Function {
	let m = new class extends Middleware{
		run(req: Request, res: Response): Promise<void> {
			return f(req, res);
		}
	};

	return m.handler();
}

// Re-Exports
export {default as Endpoint} from "./endpoint";
export {default as APIError} from "./api-error";
export {default as API} from "./api";
export {default as Middleware} from "./middleware";
export {default as Handler} from "./handler";
export * from "./cors";
