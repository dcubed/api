# D Cubed API Attacher
The API Attacher is an Express.js middleware that attaches API's in a well-defined format.


## D Cubed API Format
The D Cubed API Format is a file structure format that allows defining an API without creating files to manually attach services.
The format is intended to be as simple as possible but is only meant for APIs, not top level web application structuring.
The format is also designed to limit how much application code is placed into a single file.

### Endpoints
Endpoints are a single base URL that processes HTTP requests.
An endpoint is defined by creating a file
