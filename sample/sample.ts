import * as express from "express";
import * as api from "..";
import * as path from "path";

const app = express();
app.use('/api', api.attach(path.join(__dirname, 'api')));

app.listen(8080);
