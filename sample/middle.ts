import { Middleware } from "..";

/** This is a sample of how middleware can add properties to a request, asynchronously. */
export class SampleMiddleware extends Middleware {
	async run(req, res) {
		req.mid = {
			test: 'abc123'
		};
	}
};

declare module "express" {
	interface Request {
		mid: string;
	}
}
