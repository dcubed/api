import { handler } from "../..";
import { SampleMiddleware } from "../middle";

exports.get = handler(async (req, res) => {
	res.status(200).json(req.mid);
}).requires(new SampleMiddleware());
